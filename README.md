﻿# Cooperative API Misuse Detection Using Correction Rules

This repository provides the data sets and scripts used in the paper "Cooperative API Misuse Detection Using Correction Rules" by Sebastian Nielebock, Robert Heumüller, Jacob Krüger, and Frank Ortmeier from the Faculty of Computer Science of the Otto-von-Guericke University Magdeburg, Germany published at New Ideas and Emerging Results Track at the 42nd International Conference on Software Engineering (https://doi.org/10.1145/3377816.3381735)

All scripts and data sets were provided by Sebastian Nielebock and come without guarantee. When using the scripts and data sets please refer to the README file within the directory experimental-data-and-scripts. For any issues regarding replication do not hesitate to contact me (sebastian.nielebock <at> ovgu.de)

If you use or refer to these datasets, please cite our paper by using the following BibTex entry.

```
@inproceedings{Nielebock2020,
title = {Cooperative API Misuse Detection Using Correction Rules},
author = {Sebastian Nielebock and Robert Heum\"{u}ller and Jacob Kr\"{u}ger and Frank Ortmeier},
editor = {ACM},
year = {2020},
date = {2020-05-29},
booktitle = {Proccedings of the 42nd IEEE/ACM International Conference on Software Engineering - New Ideas and Emerging Results Track, ICSE-NIER},
publisher = {ACM},
keywords = {API Misuse, API Misuse Detection, Bug Fix, Misuse Detection, testing},
tppubtype = {inproceedings}
}
```

For our implementation, we used the following different libraries:

- MUDetect (https://github.com/stg-tud/MUDetect) which at time of publication is licensed under Mozilla Public License (MPL) Version 2.0.
- JGraphT 1.2.0 (https://github.com/jgrapht/jgrapht) licensed under GNU Lesser General Public License (LGPL) 2.1 or Eclipse Public License (EPL)
- JUnit 4.12 (https://github.com/junit-team/junit4) licensed under Eclipse Public License (EPL) - v 1.0
- log4j 2.11.1 (https://github.com/apache/logging-log4j2) licensed under Apache License, version 2.0.

This data set and the respective scripts are licensed under CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/)