#!/bin/bash

while IFS= read -r line
do
  echo "$line"
  java -jar loadSerAndCheckContainment.jar misuses single_correction_rules/$line/misuse-graph.ser | grep "Found pattern" > misuses/misuse_$line
  java -jar loadSerAndCheckContainment.jar misuses single_correction_rules/$line/correction-graph.ser | grep "Found pattern" > misuses/correction_$line 
  java -jar loadSerAndCheckContainment.jar fixes single_correction_rules/$line/misuse-graph.ser | grep "Found pattern" > fixes/misuse_$line
  java -jar loadSerAndCheckContainment.jar fixes single_correction_rules/$line/correction-graph.ser | grep "Found pattern" > fixes/correction_$line
done < "$1"