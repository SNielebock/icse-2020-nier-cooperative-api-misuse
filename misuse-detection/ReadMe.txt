To check the containment of the fixing graphs the script with

sh runContainmentCheck.sh fixing_rules

This checks in which methods of the misused java files (in directory misuses) and of the fixed java files (in directory fixes) the fixing-pattern, namely, the misused and the fixed AUG of that rule is found.
For both sets (directory misuses and fixes) for each rule result files are generated named correction_<fixing-rule-name> (for fixed AUG of the rule) and misuse_<fixing-rule-name> (for misused AUG of the rule). These files contain in which methods of the checked java files the respective subgraph has been found.